# Defining Public Key
variable "public_key" {
  default = "id_rsa.pub"
}
#Defining private key
variable "private_key" {
  default = "key.pem"
}

# Definign Key Name for connection
variable "key_name" {
  default = "davekey1"
  description = "Desired name of AWS key pair"
}
# Defining AMI
variable "ami" {
  default = {
    us-east-1 = "ami-09d56f8956ab235b3"
  }
}

# Defining Instace Type
variable "instancetype" {
  default = "t3.micro"
}

# Defining Master count 
variable "master_count" {
  default = 1
}
# Defining Region
variable "aws_region" {
  default = "us-east-1"
}
variable "subnet_id" {}
variable "security_group" {}