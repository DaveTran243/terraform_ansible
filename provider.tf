terraform {
  # required_version = ">= 1.2.2"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = " >= 3.63"
    }
  }
}

# Configure and downloading plugins for aws
provider "aws" {
  region     = "${var.aws_region}"
}

