#backend config
terraform {
  backend "s3" {
    bucket = "ductm41-demobackend"
    key    = "terraform/us-east-1/ductm41/ductm41.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-s3-backend"
  }
}
